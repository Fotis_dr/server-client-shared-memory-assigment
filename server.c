#include <stdio.h>
#include <string.h>    //strlen
#include <stdlib.h>    //strlen
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <unistd.h>    //write
#include <pthread.h> //for threading , link with lpthread
#include <sys/types.h> //key_t
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/time.h> 
 
//the thread function
void *connection_handler(void*);
pthread_mutex_t mutexl = PTHREAD_MUTEX_INITIALIZER;

pthread_t thread_id1, thread_id2;

struct shared_mem
{
    int coins;  
    pthread_mutex_t mutex;
};

struct thread_args{
	 int sock;
	 int threadflag;
};
 
int thread1_started = 0;
int thread2_started = 0;
 
int main(int argc , char *argv[])
{
    int server_sock, client_sock1 , client_sock2, client_socket[2], sd, max_sd;
	struct sockaddr_in ClntAddr1, ClntAddr2;     /* Client address */
    struct sockaddr_in server , client;
    unsigned short ServPort;         /* Server port */
	unsigned int clntLen;          
	int shmid;
	key_t key;
	char message[50];
	int serv, new_socket;
	int max_clients = 2;
	int activity;	
	fd_set readfds;
	int j = 0;
	
	for(j=0; j < max_clients ; j++){
		client_socket[j]=0;
	}
	
    if (argc != 3)     /* Test for correct number of arguments */
    {
        fprintf(stderr, "Please insert port number as first arguement, and coin number as secont\n");
        exit(-1);
    }	
	ServPort = atoi(argv[1]);
	
	
    //Create socket
    server_sock = socket(AF_INET , SOCK_STREAM , 0);
    if (server_sock == -1)
    {
        printf("Could not create socket1");
		exit(-1);
    }
    puts("Socket1 created");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(ServPort);
     
	
	//Bind
    if( bind(server_sock,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        exit(-1);
    }
    puts("bind1 done");
     
	
	 
    //Listen
    if(listen(server_sock , 2) < 0)
	{
		perror("More connections than listen queue expected, listen failed.");
		exit(-1);
	}	
    
    
          
    //Accept and incoming connection
    printf("\nServer is listening on port %d",ServPort);
	printf("\n");
    clntLen = sizeof(struct sockaddr_in);
	
	/* allocating shared memory space with permitions */
	shmid = shmget(key, sizeof(struct shared_mem), IPC_CREAT | 0666); /* read/write premitions */
	
	if(shmid < 0)
	{
		perror("shared memory allocation failed\n");
		exit(-1);
	}
	/* sh is a pointer attached to the shared memory segment */
	struct shared_mem *sh = shmat(shmid, NULL, 0);
	struct shared_mem *shared_m;
	/* casting sh into a struct pointer */
	shared_m = (struct shared_mem *)sh;
	shared_m->coins = atoi(argv[2]);
	pthread_mutex_init(&shared_m->mutex, NULL);
	/* now the shared memory seg is a type struct shared_mem */
	/* 
	 * we have to avoid malloc because this will make the shared memory
     * private and clients launched from other programs wont be able to access	 
	*/
	
    printf("coins from shared_m = %d\n",shared_m->coins);
	struct shared_mem *shared_m1;
	shared_m1 = (struct shared_mem *)sh;
	printf("coins from sh = %d\n",shared_m1->coins);
   
    /* because of problems i faced with sockets  */
    key = 9010;   
    
	int key_int = 9010;
	char key_string[10];
	/* 
	 * key was passed as a string through sockets but 
	 * sockets didnt work correcly after the shared memory implementation
	 */
	snprintf(key_string, 10 ,"%d\n", shmid);
	printf("shm id is %s\n",key_string);
    
	int connections = 0;
	int i = 0;
	while(shared_m->coins > 0)
    {
        //clear the socket set
		FD_ZERO(&readfds);
		
		//add master socket to set
        FD_SET(server_sock, &readfds);
		
		serv = server_sock;
		//add child sockets to set
        for ( i = 0 ; i < max_clients ; i++) 
        {
            //socket descriptor
            sd = client_socket[i];
             
            //if valid socket descriptor then add to read list
            if(sd > 0)
                FD_SET( sd , &readfds);
             
            //highest file descriptor number, need it for the select function
            if(sd > max_sd)
                max_sd = sd;
        }
		
		if(shared_m->coins == 0)
			break;
	
		//wait for an activity on one of the sockets , timeout is NULL , so wait indefinitely
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);
			
		int new_con = 0;
		
		while(new_con < 2){
			//If something happened on the master socket , then its an incoming connection
			if(FD_ISSET(server_sock, &readfds)) 
            {
                if ((new_socket = accept(server_sock, 
			             (struct sockaddr *)&ClntAddr1, &clntLen))<0)
                {
                        perror("accept");
                        exit(-1);
                }
          
                // inform user of socket number - used in send and receive commands
                printf("New connection , socket fd is %d , ip is : %s , port : %d \n" ,
		               	new_socket , inet_ntoa(ClntAddr1.sin_addr) , ntohs(ClntAddr1.sin_port));
        		    
                client_socket[new_con] = new_socket;
                printf("Adding to list of sockets as %d\n" , j);
                new_con++;  
		    }
		}
		/* launching the first client that connected as thread */
		if(client_socket[0] != 0 && thread1_started == 0)
		{
			thread1_started = 1;
            client_sock1 = client_socket[0];
	        struct thread_args *args1 = malloc(sizeof(struct thread_args));
		    args1->sock = client_sock1;
		    args1->threadflag = 1;
			/* args1 will be used to pass arguments into the threaded function */
			
			printf("Connection accepted with socket %d\n",client_sock1);
			
			if( (pthread_create( &thread_id1 , NULL , connection_handler
		                                    , (void*) args1)) < 0){
                    perror("could not create thread");
                    exit(-1);
            }
		    printf("thread created : %d\n",thread_id1);
		}

		/* launching the second client that connected as thread */
		if(client_socket[1] != 0 && thread2_started == 0)
		{
			thread2_started = 1;
            client_sock2 = client_socket[1];
	        struct thread_args *args2 = malloc(sizeof(struct thread_args));
		    args2->sock = client_sock2;
		    args2->threadflag = 2;
			
			printf("Connection accepted with socket %d\n",client_sock2);
			
			if( (pthread_create( &thread_id1 , NULL , connection_handler
		                                    , (void*) args2)) < 0){
                    perror("could not create thread");
                    exit(-1);
            }
		    printf("thread created : %d\n",thread_id1);
		}  
		pthread_join( thread_id1 , NULL);
		pthread_join( thread_id2 , NULL);
		printf("coins from shared_m = %d\n",shared_m->coins);
    }
	
    printf("(outer)coins from shared_m = %d\n",shared_m->coins);

	/* detaching the shared memory poiner */
	shmdt(sh);
	/* releasing the shared memory segment */
	shmctl(shmid, IPC_RMID, NULL);
	/* closing the server socket */
    close(server_sock); 
    return 0;
}
 
/*
 * This will handle connection for each client
 * */
void *connection_handler(void* arguments)
{
    //Get the socket descriptor
	struct thread_args *args =  (struct thread_args *)arguments;
	char message[10],recv_data[10];
	int bytes_recieved;
	strcpy(message, "a\n");
	
	/* otherwise random results will be the same for parallel threads */
	int seed = time(NULL);
    srand(seed);
    
	printf("arg sock is %d\n",args->sock);
	printf("arg flag is %d\n",args->threadflag);
	
    if(args->threadflag == 1){
		int sl = rand()%11+1;
		printf("will sleep for %d secs\n",sl);
		sleep(sl);
		// used to get the client running
		write(args->sock , message , strlen(message));
		
		// used for the caller to wait the client to finish
		
		if((bytes_recieved  = recv(args->sock, recv_data, strlen(recv_data)-1, 0)) < 0){	
	        perror("recv error");
		    exit(-1);
     	} 
		
		thread1_started = 0;
	}
    
	/* had in plan to use this to make team prints work */
    if(args->threadflag == 2){
		int sl = rand()%10+1;
		printf("will sleep for %d secs\n",sl);
		sleep(sl);
		write(args->sock , message , strlen(message));
		
		
		if((bytes_recieved  = recv(args->sock, recv_data, strlen(recv_data)-1, 0)) < 0){	
	        perror("recv error");
		    exit(-1);
     	}
        	
		thread2_started = 0;
	} 	
	close(args->sock);
	pthread_exit(NULL);
    return ;
}