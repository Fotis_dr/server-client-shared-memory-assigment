#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> //key_t
#include <sys/ipc.h>
#include <sys/shm.h>

struct shared_mem
{
    int coins;  
    pthread_mutex_t mutex;
};

int main(int argc, char *argv[])
{
    struct sockaddr_in server_addr;     /* server address */
    unsigned short ServPort;         /* server port */
    char *servIP;                    /* Server IP address (dotted quad) */
    char *echoString;                /* String to send to server */
    unsigned int echoStringLen;      /* Length of string to echo */
    int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv() 
                                        and total bytes read */
    char message[10], recv_data[10];
	strcpy(message, "a\n");
    if (argc != 3)    
    {
       fprintf(stderr, "Please insert port number as first arguement and shared memory id as secont\n");
       exit(-1);
    }
    ServPort = atoi(argv[1]); 

    int socket_desc;
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
		exit(-1);
    }
    puts("Socket created");
    
	
	server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); /* localhost ip */
    server_addr.sin_port = htons(ServPort);
    
    if (connect(socket_desc, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
        printf("connect() failed");
	    exit(-1);
    }
	
	key_t key;
	int shmid = atoi(argv[2]);
	int bytes_recieved ;
	void *sh ;
    
	if((bytes_recieved  = recv(socket_desc, recv_data, strlen(recv_data)-1, 0)) < 0){	
	    perror("recv error");
		exit(-1);
	} 
	/* this would be used to grab the key */
	/*
	recv_data[bytes_recieved] = '\0';
    printf("recv data is %s\n",recv_data);
    int testshmid = atoi(recv_data);
    printf("shmid value = %d\n", shmid);
	*/
	
	/* didnt work so... */
	key = 9010;
    
	/*   tried to use  
	  shmid = shmget(key, sizeof(struct shared_mem), 0666);
	  to grab the shmid but it caused some problems too 
	*/
		
	/* attaching the shared memory segment to a pointer */		
    sh =(struct shared_mem *) shmat(shmid, NULL, 0);		
		
	struct shared_mem *shm;
	shm = (struct shared_mem *)sh;
	/*
     * casting it to (struct shared_mem *) so we can 
	 * access the arguments of the struct (in this case the shared memory)
	 */	
		
	printf("(old)shared memory is : %d\n", shm->coins);
	
	int seed = time(NULL);
    srand(seed);
	
	fflush(stdout);
	int i = 0;
	int new_value;
	int total_removed = 0;
	
	while(shm->coins > 0){
		int sl = rand()%10+1; // random from 1 to 10
	    printf("will sleep for %d secs\n",sl);
	    sleep(sl);
		
		int rand_coins = rand()%10+1;
		printf("will try to remove %d coins\n",rand_coins);
		
		/* critical section !!*/
		pthread_mutex_lock(&shm->mutex);
		new_value = shm->coins;
		if(new_value == 0){
			write(socket_desc , message , strlen(message));
			shmdt(sh);
            close(socket_desc);
	        return 0;
		}
		if(rand_coins > new_value){
			rand_coins = new_value;
		}
		total_removed += rand_coins;
		new_value = new_value - rand_coins;
		shm->coins = new_value;
		printf("(new)shared memory is : %d\n", shm->coins);
		printf("this team has removed %d coins so far\n",total_removed);
		pthread_mutex_unlock(&shm->mutex);
		/* end of critical section */
	}
	
	write(socket_desc , message , strlen(message));    	  
	shmdt(sh);
    close(socket_desc);
	return 0;
}